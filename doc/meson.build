# Copyright © 2020-2021 Collabora, Ltd.
# Author: Antonio Caggiano <antonio.caggiano@collabora.com>
#
# SPDX-License-Identifier: MIT

doxygen = find_program('doxygen', required: false)
if not doxygen.found()
    error('Skipping documentation: doxygen not found')
else
    config_data = configuration_data()
    config_data.set('VERSION', meson.project_version())
    config_data.set('ROOT', meson.source_root())
    config_data.set('BUILD', meson.build_root())

    doxyfile = configure_file(input: 'Doxyfile.in',
                              output: 'Doxyfile',
                              configuration: config_data,
                              install: false)

    doc_dir = join_paths(meson.build_root(), 'doc', 'gfx-pps')

    html_target = custom_target('gfx-pps-doc',
                                input: doxyfile,
                                output: 'html',
                                command: [doxygen, doxyfile],
                                install: true,
                                install_dir: doc_dir)
endif
