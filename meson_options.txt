option('gpu', type: 'boolean', value: true, description: 'Whether to build gpu producer')
option('test', type: 'boolean', value: true, description: 'Whether to build tests')
option('doc', type: 'boolean', value: false, description: 'Whether to build documentation')
