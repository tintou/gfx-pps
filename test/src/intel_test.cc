/*
 * Copyright © 2020-2021 Collabora, Ltd.
 * Author: Antonio Caggiano <antonio.caggiano@collabora.com>
 *
 * SPDX-License-Identifier: MIT
 */

#include <cstring>

#include <gtest/gtest.h>
#include <perf_data_reader.h>

#include <pps/algorithm.h>
#include <pps/pds.h>

#include <pps/gpu/intel/intel_driver.h>

namespace pps::gpu
{
std::optional<IntelDriver> init_concrete_driver()
{
    auto drm_device_opt = DrmDevice::create(0);
    if (!drm_device_opt) {
        return std::nullopt;
    }

    auto &drm_device = drm_device_opt.value();
    auto driver = IntelDriver();
    driver.drm_device = std::move(drm_device);
    driver.init_perfcnt();
    driver.enable_all_counters();

    constexpr uint64_t sampling_period_ns = 1000000;
    driver.enable_perfcnt(sampling_period_ns);

    return driver;
}

/// @brief Verify GPU timestamps read from the driver can be transformed into
/// CPU timestamps and that the timing order is correct and preserved
TEST(Intel, GpuTimestamps)
{
    auto driver = init_concrete_driver();
    if (!driver) {
        GTEST_SKIP_("Failed to initialize concrete driver");
    }

    uint32_t prev_gpu_ts = 0;
    uint64_t prev_cpu_ts = 0;
    for (auto iterations = 32; iterations != 0; --iterations) {
        // Dump until we can read some records
        while (!driver->dump_perfcnt()) { }

        uint32_t sample_count = 0;
        while (auto gpu_timestamp = driver->gpu_next()) {
            auto cpu_timestamp = driver->correlate_gpu_timestamp(gpu_timestamp);

            EXPECT_NE(cpu_timestamp, 0);
            EXPECT_GT(cpu_timestamp, prev_cpu_ts);
            EXPECT_GT(gpu_timestamp, prev_gpu_ts);

            prev_gpu_ts = gpu_timestamp;
            prev_cpu_ts = cpu_timestamp;
            sample_count++;
        }

        EXPECT_GT(sample_count, 0);
    }
}

/// @brief Make sure correlations timestamps are valid and that CPU timestamps
/// calculated by the driver fall between first and last correlation
TEST(Intel, Correlations)
{
    auto driver = init_concrete_driver();
    if (!driver) {
        GTEST_SKIP_("Failed to initialize concrete driver");
    }

    // Dump until we can read some records
    while (!driver->dump_perfcnt()) { }

    // Get first correlation
    auto corr_a = driver->correlations[0];

    // Get first sample
    uint64_t cpu_timestamp = driver->next();
    EXPECT_GT(cpu_timestamp, corr_a.cpu_timestamp);

    // Get last correlation
    auto corr_b = driver->correlations.back();
    EXPECT_LT(cpu_timestamp, corr_b.cpu_timestamp);

    driver->disable_perfcnt();
}

std::vector<uint8_t> read_data_from_file(const char *path)
{
    auto file = std::fopen(path, "rb");
    EXPECT_TRUE(file != nullptr);

    std::fseek(file, 0, SEEK_END);
    size_t file_size = std::ftell(file);
    EXPECT_GT(file_size, 0);
    std::fseek(file, 0, SEEK_SET);

    std::vector<uint8_t> data = {};
    data.resize(file_size);
    size_t bytes_read = std::fread(data.data(), sizeof(data[0]), file_size, file);
    EXPECT_EQ(bytes_read, file_size);

    return data;
}

/// @brief Intel driver should be able to read perf records from data
TEST(Intel, PerfCnt)
{
    // Dummy driver with no DRM device in it
    auto driver = IntelDriver();
    auto data = read_data_from_file("test/data/gpu/intel/intel.dump");
    auto records = driver.parse_perf_records(data, data.size());
    EXPECT_GT(records.size(), 0);
}

} // namespace pps::gpu

int main(int argc, char **argv)
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
